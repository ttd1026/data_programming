import py2neo
from multiprocess import parallel_by_function
from py2neo import Graph
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import csv
from sympy.physics.units import frequency

py2neo.authenticate("localhost:7474", "neo4j", "classroom")
graph = Graph("http://localhost:7474/db/data/")

#Used to get tokenized string.
# The following function was written by Professor Ling in our
# Unstructured Data Class. The code was provided as part of our lessons.
def getWordList(text, word_proc = lambda x:x):
    word_list = []
    for sent in sent_tokenize(text):
        for word in word_tokenize(sent):
            word_list.append(word)
    return word_list 

#Used to get tokenized string.
def frequency_search(query_text):
    
    #Cleans and tokenizes query text.
    # The following four lines of code were also written by Professor Ling
    # In our unstructured data class.
    stemmer = SnowballStemmer('english')
    
    word_list = getWordList(query_text, lambda x: x.lower())
    word_list = [word for word in word_list if not word in stopwords.words('english')]
    word_list = [stemmer.stem(word) for word in word_list]
    
    #Finds every product each word in the query matches to.
    results = []
    for word in word_list:
        
        query = """MATCH (w:Word {text:'""" + word + """'})-[r:OCCURS_IN]->(p)
                    RETURN p.sku"""
        
        results.append(graph.run(query))
    
    #Append each sku in the results to a list.
    sku_list = []
    for result in results:
        sku_add = None
        for item in result:
            if sku_add is None:
                sku_add = set([item['p.sku']])
            else:
                sku_add.add(item['p.sku'])
        if sku_list is None:
            if sku_add is not None:
                sku_list = sku_add
        else:
            if sku_add is not None:
                sku_list.append(sku_add)
    
    #Find the overlapping skus between each word in the query. If the sku lists
    # do not overlap, combine the sets.
    if sku_list == []:
        return " ", { }
    
    sku_set = sku_list[0]
    for index,entry in enumerate(sku_list):
        if index != 0 and entry != set() and entry is not None:
            if sku_set & entry == set():
                sku_set |= entry
            else:
                sku_set &= entry
    
    #Find the freaquency at which each sku is clicked on in total by
    # each word in the query. It also determines how relevant each sku is
    # based on whether each word in the query is in the product name.
    name_relevance= {}
    frequency = {}
    for sku in sku_set:
        query = """ MATCH (p:Product {sku:'""" + sku + """'})
                    RETURN p.name"""
        
        results = graph.run(query)
        for result in results:
            name = result["p.name"]
        
        relevance = 0
        frequent = 0.0
        for word in word_list:
            
            query = """ MATCH (p:Product {sku:'""" + sku + """'}) - [r:OCCURS_IN] - (w:Word {text:'""" + word + """'})
                    RETURN r.frequency"""
        
            results1 = graph.run(query)
            
            for result1 in results1:
                frequent += float(result1['r.frequency'])
            
            if word in str(name).lower():
                relevance += 1
        frequency[sku] = frequent
        name_relevance[sku] = relevance
    
    #Create a list of the most relevant skus.
    values = []
    for key,value in name_relevance.items():
        values.append(value)
    max_ = max(values)
    
    highest = []
    final_dict = {}
    for key,value in frequency.items():
        if max_ == name_relevance[key]:
            final_dict[key] = value
            if highest == []:
                highest = [key,value]
            elif value > highest[1]:
                highest = [key,value]
    
    # Return from the most relevant skus both the highest ranking sku based
    # on the number of times the total sku was chosen and a dictionary
    # of each of the most relevant skus and their number of clicks.
    return highest[0], final_dict

# Function to demonstrate returning a product from a query
# based on the above function.
def demo():
    while True:
        string = input("Enter a query: ")
        sku, dict_ = frequency_search(string)
        
        query = """ MATCH (p:Product {sku:'""" + sku + """'})
                            RETURN p.name"""
                
        results = graph.run(query)
        
        for result in results:
            print(result['p.name'])

def demo_accuracy():
    with open("C:\\Users\\Kyle\\Desktop\\Data_Programming_Project\\train.csv", encoding='utf-8') as csv_file:
        csv_string = csv_file.read()
        csv_reader = csv.reader(csv_string.splitlines(), delimiter=',', quotechar='"')
            
        correct = 0
        for index,row in enumerate(csv_reader):
            if index != 0:
                answer,dict_ = frequency_search(row[3])
                if answer == row[1]:
                    correct += 1
                    print('correct')
                else:
                    print('wrong')
                if index == 1000:
                    break
        print(correct/(index +1))
        


demo()
#demo_accuracy()

