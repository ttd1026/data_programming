import py2neo
import os
import time
from multiprocess import parallel_by_function
from csv_parser import csvParser
from py2neo import Graph
from xml_parser import xmlParser
from xml_review_parser import ReviewToCsv
from upload_word_count import upload_word_count

"""
The following script drives the data injestion process, pulling the necessary data from csv and xm
files into the neo4j database. The entire process takes about 30 minutes.
"""

# Function used to load processed categories to the database.
def upload_categories(xmlParser_object, graph):
    
    query = '''LOAD CSV WITH HEADERS FROM "file:///''' + xmlParser_object.file_name + '''" AS line
                CREATE (c:Category { catID: line.catID, catName: line.catName})
                '''

    graph.run(query)
    
    graph.run("""CREATE CONSTRAINT ON (category:Category) ASSERT category.catID IS UNIQUE""")
    
# Function used to load catagories relationships to database.
def upload_subcategores(xmlParser_object, graph):
    
    query = '''LOAD CSV WITH HEADERS FROM "file:///''' + xmlParser_object.file_name1 + '''" AS line
                MATCH (p:Category {catID: line.parent}),(c:Category {catID: line.child})
                CREATE UNIQUE (p)<-[:SUBCATEGORY_OF]-(c)
                '''

    graph.run(query)

# Function used to upload processed catagories to the database.
def upload_products(xmlParser_object, graph):
    
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + xmlParser_object.file_name + '''" AS line
                CREATE (p:Product {sku: line.sku, name: line.name, description: line.description, date: line.date,
                        salePrice: line.salePrice, regularPrice: line.regularPrice, onSale: line.onSale, preOwned: line.preOwned, inStore: line.inStore})'''
    
    graph.run(query)
    
    query = '''CREATE CONSTRAINT ON (product:Product) ASSERT product.sku IS UNIQUE'''
    
    graph.run(query)
                
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + xmlParser_object.file_name + '''" AS line
                MATCH (p:Product {sku: line.sku}),(c:Category {catID: line.category})
                CREATE (p)-[:BELONGS_TO]->(c)'''

    graph.run(query)
    
# Function used to upload users to the database.
def upload_users(csvParser_object, graph):
    
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + csvParser_object.file_name + '''" AS line
                CREATE (p:User {userID: line.userID})'''
    
    graph.run(query)
    
    query = '''CREATE CONSTRAINT ON (user:User) ASSERT user.userID IS UNIQUE'''
    
    graph.run(query)

# Function used to upload queries to the database.
def upload_queries(csvParser_object, graph):
    
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + csvParser_object.file_name + '''" AS line
                CREATE (q:Query {queryID: line.queryID, queryText: line.query, queryTime: line.query_time})'''
    
    graph.run(query)
    
    query = '''CREATE CONSTRAINT ON (query:Query) ASSERT query.queryID IS UNIQUE'''
    
    graph.run(query)
                
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + csvParser_object.file_name + '''" AS line
                MATCH (q:Query {queryID: line.queryID}),(u:User {userID: line.userID})
                CREATE (u)-[:EXECUTED]->(q)'''
        
    graph.run(query)
    
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + csvParser_object.file_name + '''" AS line
                MATCH (q:Query {queryID: line.queryID}),(p:Product {sku: line.sku})
                CREATE (q)-[:CLICKED_ON {clickTime: line.click_time}]->(p)'''
            
    graph.run(query)

# Function used to process product information and write to in memory csv.
def process_products(path):
    product = xmlParser(path, neo_import_path)
    product.pro_product()
    product.create_memcsv()
    
    return product
    
# Function to write in memory csv to file system.
def write_products(xmlParser_object):
    xmlParser_object.write_memcsv()
    
    return xmlParser_object

'''
Beginning uploading review xml
'''    
def upload_sku(file_name, graph):
    
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + file_name + '''" AS line
                MERGE (p:Product {sku: line.sku})'''
    graph.run(query)
    
    
def upload_review(file_name, graph):
    
    graph.run('''CREATE CONSTRAINT ON (r:Review)
                    ASSERT r.review_id IS UNIQUE''')
    
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + file_name + '''" AS line
                MERGE (r:Review {review_id: line.review_id,
                                  rating: toInt(line.rating),
                                  comment: line.comment})'''

    graph.run(query)


def upload_review_sku_relationship(file_name, graph):
    
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + file_name + '''" AS line
                MATCH (p:Product {sku: line.sku}), (r:Review {review_id: line.review_id})
                MERGE (r)-[:ABOUT]->(p)'''
    
    graph.run(query)
'''
End uploading review xml 
'''

# Variables with file paths to data.
py2neo.authenticate("localhost:7474", "neo4j", "classroom")
graph = Graph("http://localhost:7474/db/data/")
query_path = "C:\\Users\\Kyle\\Desktop\\Data_Programming_Project\\train.csv"
cat_path = "C:\\Users\\Kyle\\Desktop\\Data_Programming_Project\\product_data.tar\\product_data\\categories\\categories_0001_abcat0010000_to_pcmcat99300050000.xml"
review_path = "C:\\Users\\Kyle\\Desktop\\Data_Programming_Project\\product_data.tar\\product_data\\reviews"
prod_path = "C:\\Users\\Kyle\\Desktop\\Data_Programming_Project\\product_data.tar\\product_data\\products"
review_path = "E:\\Documents\\GSU\\CIS 8005 Data Programming\\Project\\product_data\\reviews"
neo_import_path = "C:\\Users\\Kyle\\Documents\\Neo4j\\classProject\\import\\"

if __name__ == "__main__":
    
    start_time = time.time()  # Used to benchmark time to complete entire script.
    
    #Creates a Neo4j Import Directory, if it does not exist.
    if not os.path.exists(neo_import_path):
        os.makedirs(neo_import_path)
    
    #Add Categories to database.
    print("Adding Categories")
    categories = xmlParser(cat_path,neo_import_path)
    
    print("   Processing Categories")
    categories.pro_categories()
    categories.create_memcsv()
    
    print("   Writing Processed Categories to CSV")
    categories.write_memcsv()
    
    print("   Uploading Processed Categories to Neo4j")
    upload_categories(categories, graph)
    upload_subcategores(categories,graph)
    
    #Add Products to database
    print('Adding Products')
    
    # Used to make a list of xml files to parse.
    file_list = os.listdir(prod_path)
    final_list = []
    for file in file_list:
        final_list.append(os.path.join(prod_path,file))

    data_sets = final_list
    print("   Processing Products")
    functions = [process_products]
    data_sets = parallel_by_function(data_sets, functions, cores=4, chunk=False)
    print("   Writing Processed Products to CSV")
    functions = [write_products]
    
    # Only one core is used to make sure that only one process is opening the csv at a time.
    data_sets = parallel_by_function(data_sets, functions, cores=1, chunk=False)
    print("   Uploading Processed Products to Neo4j")
    upload_products(data_sets[0],graph)
    
    
    #Add Users to database
    print('Adding Users')
    users = csvParser(query_path,neo_import_path)
    print("   Processing Users")
    users.pro_users()
    users.create_memcsv()
    print("   Writing Processed Users to CSV")
    users.write_memcsv()
    print("   Uploading Processed Users to Neo4j")
    upload_users(users, graph)
    
    #Add Queries to database
    print('Adding Queries')
    print("   Processing Queries")
    queries = csvParser(query_path,neo_import_path)
    queries.pro_queries()
    print("   Uploading Processed Queries to Neo4j")
    upload_queries(queries,graph)
    

    # Parse reviews to database

        # Parse reviews to database

    file_name = "review_out.csv"
    review = ReviewToCsv(review_path, 
                         os.path.join(neo_import_path, file_name))
    print("Parsing Reviews")
    review.get_review()
    
    print("   Writing to csv")
    review.write_review_to_csv()
    

    # Upload reviews to database 
    print("Uploading sku's")

    print("   Uploading SKUs")

    # Upload reviews to database 
    print("   Uploading sku's")

    upload_sku(file_name, graph)
    
    print('   Uploading reviews')
    upload_review(file_name, graph)
    
    print("   Uploading relationships")
    upload_review_sku_relationship(file_name, graph)
    
    #Add Word Count
    print("Producing Word Count")
    upload_word_count()
    
    end_time = time.time()
    print("---- Upload completed successfully in {} minutes ----".format(round((end_time - start_time)/60,2)))
