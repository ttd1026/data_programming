import py2neo
import csv
import io
import os
import shutil
import xml.etree.ElementTree as ET
from py2neo import Graph

py2neo.authenticate("localhost:7474", "neo4j", "classroom")
graph = Graph("http://localhost:7474/db/data/")

# xmlParser object with functions to store xml data in memory, process specific datasets
# to be injested into the database, and contains functions to write csv to memory and then to
# disk. Individual csvs are combined in memory before being written to the disk rather
# than writing multiple files to the disk.
class xmlParser():
    def __init__(self,file_path="",output_path=""):
        self.in_path = file_path
        self.out_path = output_path
        self.data = None
        self.data_subcat = None
        self.data_headers = None
        self.file_name = None
        self.file_name1 = None
        
    def create_memcsv(self):
        output = io.StringIO()
        writer = csv.writer(output, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(self.data_headers)

        for row in self.data:
            writer.writerow(row)
            
        self.data = output
        
        if self.data_subcat is not None:
            output1 = io.StringIO()
            writer = csv.writer(output1, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(self.data_subcat_headers)
            for row in self.data_subcat:
                writer.writerow(row)
                
            self.data_subcat = output1
        
    def write_memcsv(self):
        file_path = os.path.join(self.out_path, self.file_name)
       
        if os.path.exists(file_path):
            with open (file_path, 'a', encoding='utf-8') as output_file:
                self.data.seek (0)
                self.data.readline()
                shutil.copyfileobj (self.data, output_file)
        
        else:    
            with open (file_path, 'w', encoding='utf-8') as output_file:
                self.data.seek (0)
                shutil.copyfileobj (self.data, output_file)
            
            if self.data_subcat is not None:
                self.file_name1 = "sub_" + self.file_name
                file_path = os.path.join(self.out_path, self.file_name1)
                with open (file_path, 'w', encoding='utf-8') as output_file:
                    self.data_subcat.seek (0)
                    shutil.copyfileobj (self.data_subcat, output_file)
    
    # Process Categories
    def pro_categories(self):
        categories = []
        sub_categories = []
        skip_list = ['categories','category','path','subCategories']
        
        tree = ET.parse(self.in_path)
        root = tree.getroot()
        
        # Parse XML
        for child in root.iter():
            tag = child.tag
            text = child.text
            
            if child.tag not in skip_list:
                if tag == 'id':
                    categories.append([text])
                if tag == 'name':
                    categories[len(categories)-1].append(text)
            
            if child.tag == 'subCategories' and child.text != "":
                for sub in child.iter():
                    if sub.tag not in skip_list:
                        tag = sub.tag
                        text = sub.text
                        
                        if tag == 'id':
                            sub_categories.append([categories[len(categories)-1][0]])
                            sub_categories[len(sub_categories)-1].append(text)
        
        #Make sure there are no duplicate categories
        id_list = []
        new_list = []
        for entry in categories:
            if entry[0] not in id_list:
                id_list.append(entry[0])
                new_list.append(entry)

        self.data = new_list
        self.data_subcat = sub_categories
        self.data_headers = ["catID", "catName"]
        self.data_subcat_headers = ["parent","child"]
        self.file_name = "categories.csv"

    # Process products
    def pro_product(self):
        include_list = ['sku','name','longDescription','startDate','salePrice','regularPrice','onSale','preowned','inStoreAvailability']
        tree = ET.parse(self.in_path)
        root = tree.getroot()
        
        product_list = []
        for child in root.findall('product'):
            
            properties = []
            for item in include_list:
                try:
                    properties.append(child.find(item).text)
                except AttributeError:
                    properties.append("")
                    
            # Logic to locate categories for products.
            cats = []
            cat_path = child.find('categoryPath')
            cat_list = cat_path.findall('category')
            for cat in cat_list:
                cats.append(cat.find('id').text)
            
            # Some products in the database are test products and
            # have no categories. Replace those lists with
            # empty strings so they do not raise an error later.
            if cats != []:
                output = cats[len(cats) - 1]
                if output == []:
                    cats = ""
                else:
                    cats = output
            else:
                cats = ""
            
            properties.append(cats)
            product_list.append(properties)
    
        self.data = product_list
        self.data_headers = ["sku","name","description","date","salePrice","regularPrice","onSale","preOwnded","inStore","category"]
        self.file_name = "products.csv"
