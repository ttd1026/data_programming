'''
xml parser: map an xml file, with tree and root

Documentation can be found at 
https://docs.python.org/3.5/library/xml.etree.elementtree.html#xml.etree.ElementTree.XMLParser.feed

'''
'''
parsing xml review files and write to neo4j
'''
'''
This file parsed a folder of xml file and write out to a single csv file.

Two 2 arguments are in_folder_path and csv_out_path
    - in_folder_path: the input folders that contains the xml files
    that need to be parsed.
    - csv_out_path: the path for the output csv file. It's also served as the import path
    for neo4j if used with neo4j
    
'''

import xml.etree.ElementTree as ET
import csv
import os

class ReviewToCsv():
    def __init__(self, in_folder_path, csv_out_path):
        self.in_folder_path = in_folder_path
        self.csv_out_path = csv_out_path
        
    def get_review(self):
        review_list = []
        
        for filename in os.listdir(self.in_folder_path):
            file_path = os.path.join(self.in_folder_path, filename)
            
            tree = ET.parse(file_path)
            root = tree.getroot()
           
            for review in root.findall('review'):
                sku = review.find('sku').text
                review_id = review.find('id').text 
                rating = review.find('rating').text
                comment = review.find('comment').text 
                row = [sku, review_id, rating, str(comment).encode('utf-8')]
                
                review_list.append(row)
        
        return review_list
    
    
    def write_review_to_csv(self):
        
        header = ['sku', 'review_id', 'rating', 'comment']
        with open(self.csv_out_path, 'w', newline = '') as output:
            review_writer = csv.writer(output)
            review_writer.writerow(header)
            for review in ReviewToCsv.get_review(self):
                review_writer.writerow(review)
        
        output.close()
