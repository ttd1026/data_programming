import py2neo
import os
import time
import csv
from multiprocess import parallel_by_function
from py2neo import Graph
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk import sent_tokenize, word_tokenize
from nltk.corpus import stopwords

py2neo.authenticate("localhost:7474", "neo4j", "classroom")
graph = Graph("http://localhost:7474/db/data/")
query_path = "C:\\Users\\Kyle\\Desktop\\Data_Programming_Project\\train.csv"
cat_path = "C:\\Users\\Kyle\\Desktop\\Data_Programming_Project\\product_data.tar\\product_data\\categories\\categories_0001_abcat0010000_to_pcmcat99300050000.xml"
prod_path = "C:\\Users\\Kyle\\Desktop\\Data_Programming_Project\\product_data.tar\\product_data\\products"
neo_import_path = "C:\\Users\\Kyle\\Documents\\Neo4j\\classProject\\import\\"

#Used to create a tokenized word list of each query.
def getWordList(text, word_proc = lambda x:x):
    word_list = []
    for sent in sent_tokenize(text):
        for word in word_tokenize(sent):
            word_list.append(word)
    return word_list 

#Used to get each product and it's realated category.
def get_records():
    query = """MATCH (p:Product)-[:BELONGS_TO]-(c:Category)
                RETURN p.sku,c.catID
                """
                
    results = graph.run(query)
    product_list=[]
    for result in results:
        product_list.append([result['c.catID'],result['p.sku']])
    return product_list
    
#Used to get the product each query clicked on and append it to the product list. (High number of transactions
#on neo4j, but lower amount of ram.
def get_text(product_list):
    for index,product in enumerate(product_list):
        query = """MATCH (q:Query)-[:CLICKED_ON]-(p:Product {sku:'""" + product[1]+"""'})
                RETURN q.queryText"""
        results = graph.run(query)
        for result in results:
            product.append(result['q.queryText'])
        product_list[index] = product
    return product_list

#Used to get the product each query clicked on and append it to the product list.
#Low number of transactions on neo4j, high amount of ram.
def get_text_alt(product_list):
    query = """MATCH (q:Query)-[:CLICKED_ON]-(p:Product)
                RETURN p.sku,q.queryText"""
    results = graph.run(query)
    
    query_dict = {}
    query_text = []
    
    for result in results:
        if result['p.sku'] in query_dict:
            index = query_dict[result['p.sku']]
            query_text[index].append(result['q.queryText'])
        else:
            query_text.append([result['q.queryText']])
            query_dict[result['p.sku']] = len(query_text) - 1
            
    for indx,product in enumerate(product_list):
        if product[1] in query_dict:
            index = query_dict[product[1]]
            for query in query_text[index]:
                product.append(query)
                product_list[indx] = product
    
    return product_list
    
#Used to clean and tokenize text from query.
def analyze_text(product_list):
    stemmer = SnowballStemmer('english')
    
    new_product_list = []
    for product in product_list:
        
        word_count = {}
        for index,string in enumerate(product):
            if index > 1:
                word_list = getWordList(string, lambda x: x.lower())
                word_list = [word for word in word_list if not word in stopwords.words('english')]
                word_list = [stemmer.stem(word) for word in word_list]
                               
                for word in word_list:
                    if word in word_count:
                        word_count[word] += 1
                    else:
                        word_count[word] = 1
                
        new_row = [product[0],product[1],word_count]
        new_product_list.append(new_row)
        
    return new_product_list

# Uploads the word counts to the database.
def upload_word_count():
    
    records = get_records()
    
    data_sets = [records]
    functions = [get_text_alt,analyze_text]
    data_sets = parallel_by_function(data_sets, functions, cores=4, chunk=True)
    
    word_count_rows = [['sku','word','count']]
    word_list = []
    
    for data in data_sets:
        for key,value in data[2].items():
            row = [data[1],key,value]
            word_count_rows.append(row)
            word_list.append((key))
    
    word_list = list(set(word_list))
    word_list_rows = [['word']]
    for word in word_list:
        word_list_rows.append([word])
    
    with open(os.path.join(neo_import_path,"dict_list.csv"), 'w') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerows(word_count_rows)
        
    with open(os.path.join(neo_import_path,"word_list.csv"), 'w') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerows(word_list_rows)
    
    print("   Uploading Processed Data to Database")
    query = '''USING PERIODIC COMMIT 10000
                LOAD CSV WITH HEADERS FROM "file:///''' + "word_list.csv" + '''" AS line
                CREATE (w:Word {text: line.word})'''
    
    graph.run(query)
    
    graph.run("""CREATE CONSTRAINT ON (w:Word) ASSERT w.text IS UNIQUE""")
    
    query = '''LOAD CSV WITH HEADERS FROM "file:///''' + "dict_list.csv" + '''" AS line
                MATCH (w:Word {text: line.word}),(p:Product {sku: line.sku})
                CREATE UNIQUE (p)<-[:OCCURS_IN {frequency: line.count}]-(w)
                '''
    
    graph.run(query)
