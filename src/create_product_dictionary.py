from sklearn.feature_extraction.text import CountVectorizer
import path_config
import codecs
import py2neo
from py2neo import Graph
import string

def run():
    py2neo.authenticate("localhost:7474","neo4j","neo4j1")
    graph = Graph("http://localhost:7474/db/data/")
    result=graph.data('''MATCH (n:Product)-[r:BELONGS_TO]->(c:Category) WITH n, rand() AS number RETURN n.name,n.description,n.catName order by number limit 3000''')
    st = ""

    for x in result:
        p=','.join(str(val).strip(string.punctuation) for (key,val) in x.items())
        st=st + p
        p=""
    vectorizer = CountVectorizer(strip_accents='ascii')
    tokenizer = vectorizer.build_tokenizer()
    preprocessor = vectorizer.build_preprocessor()

    tokens = set()
    

    for item in tokenizer(st):
        tokens.add(preprocessor(item))

    with codecs.open(path_config.PERSONAL_WORD_DICTIONARY_FILE, mode='wb', encoding='utf-8') as f:
        for token in tokens:
            f.write(token + '\n')


if __name__ == '__main__':
    import time
    start = time.time()
    run()
    end = time.time()
    print ("Runtime: %f seconds" % (end - start))
