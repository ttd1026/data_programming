import path_config
import numpy
import pickle
import pull_data

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC
import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
def learn(training_data, training_labels, show_score=False, store=False):

    print ("Start Learning....")
    
    clf = SVC(kernel='linear', probability=True, C=1)

    clf.fit(training_data, training_labels)

    print ("Done Learning.")
    
    
    if store:
        print ("Pickling classifier...")
        pickle.dump(clf, open(path_config.CLASSIFIER_PICKLING_FILE, 'wb'))
        print ("Done Pickling.")

    if show_score:
        print ("Scoring classifier ...")
        print ("Data-Level Training Set Prediction Accuracy: %s" % clf.score(training_data, training_labels))

def construct_tf_idf_matrix(data, store=False):

    print ("TF-IDF Normalized Matrix Construction...")

    vectorizer = TfidfVectorizer(stop_words='english')
    print(data)
    training_data = vectorizer.fit_transform(data)

    print ("Done Constructing Matrix")
    print(training_data.toarray())
    if store:
        print ("Pickling Trained Transformer...")
        pickle.dump(vectorizer, open(path_config.TRANSFORMER_PICKLING_FILE, 'wb'))
        print ("Pickling Done.")

    return training_data


def create_label_mapping(labels, store=False):
   
    mapping = dict()
    reverse_mapping = dict()

    for i, value in enumerate(set(labels)):
        mapping[value] = i
        reverse_mapping[i] = value

    new_labels = numpy.zeros(shape=len(labels), dtype=numpy.int)
    for i, value in enumerate(labels):
        new_labels[i] = mapping[value]
    
    if store:
        print ("Pickling Label to SKU Mapping...")
        pickle.dump(reverse_mapping, open(path_config.LABEL_TO_SKU_MAPPING_PICKLING_FILE, 'wb'))
        print ("Pickling Done.")

    return new_labels


def run(store=False, predefined_size=-1, show_score=False):

    data, labels = pull_data.load_training_data()#change for testing
    print ("Samples: %d" % len(data))

    training_labels = create_label_mapping(labels, store)

    training_data = construct_tf_idf_matrix(data, store)
    print ("Training Matrix size: %s x %s" % training_data.shape)

    learn(training_data, training_labels, show_score, store)


if __name__ == '__main__':
    import time
    
    start = time.time()
    store = True
    size = -1
    show_score = True

    
    run(store, size, show_score)

    end = time.time()
    print ("Runtime: %f seconds" % (end - start))
