import path_config
import numpy
import pickle
import pull_data
import push_data


def make_predictions(data, number_of_predictions):
    print ("Classifying data using %s" % path_config.CLASSIFIER_PICKLING_FILE)

    classifier = pickle.load(open(path_config.CLASSIFIER_PICKLING_FILE, 'rb'))

    print ("Assuming I need to use the label-To-SKU mapper located at: %s" % path_config.LABEL_TO_SKU_MAPPING_PICKLING_FILE)

    label_to_sku_mapping = pickle.load(open(path_config.LABEL_TO_SKU_MAPPING_PICKLING_FILE, 'rb'))

    if number_of_predictions > len(label_to_sku_mapping):
        raise Exception("""Too many predictions requested. \nRequested: %s\nAvailable: %s """ % (number_of_predictions, len(label_to_sku_mapping)))

    predictions = classifier.predict_proba(data)

    predictions_to_return = numpy.zeros(shape=(data.shape[0],number_of_predictions), dtype=numpy.uint64)

    for i in range(predictions.shape[0]):
        values_to_indices = dict( ( (value, j) for j, value in enumerate(predictions[i]) ) )
        sorted_keys = sorted(values_to_indices.keys(), reverse=True)

        for j in range(number_of_predictions):
            prediction = values_to_indices[sorted_keys[j]]
            predictions_to_return[i, j] = label_to_sku_mapping[prediction]
    
    
    return predictions_to_return


def construct_tf_idf_matrix(data):

    print ("Transforming data using %s" % path_config.TRANSFORMER_PICKLING_FILE)

    transformer = pickle.load(open(path_config.TRANSFORMER_PICKLING_FILE, 'rb'))

    transformed_data = transformer.transform(data)
    
    print ("Transformation Complete")

    return transformed_data


def run(number_of_predictions):
    print ("Number of Predictions: %s" % number_of_predictions)

    raw_testing_data = pull_data.load_testing_data()#change for testing
    print ("Samples: %s" % len(raw_testing_data))

    testing_data = construct_tf_idf_matrix(raw_testing_data)
    print ("Testing Matrix size: %s x %s" % testing_data.shape)

    prediction_data = make_predictions(testing_data, number_of_predictions)

    push_data.write_prediction_file(prediction_data)


if __name__ == '__main__':
    import time
    start = time.time()
    
    predictions = 5 #Number of predictions to make.

    run(predictions)

    end = time.time()
    print ("Runtime: %f seconds" % (end - start))
