import path_config

def write_prediction_file(predictions):

    print ("Writing predictions to: %s" % path_config.TESTING_PREDICTIONS_FILE)
    
    with open(path_config.TESTING_PREDICTIONS_FILE, 'w') as prediction_file:
        for row in predictions:
            prediction_file.write(' '.join(str(value) for value in row) + '\n')
        
    print ("Writing Finished.")

