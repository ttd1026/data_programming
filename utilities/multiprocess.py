"""
Parallel By Function

A function which can be used to run the same function against multiple datasets simultaneously.
Each data set should be an element of a list. The same is true for functions. The following is an example:

   functions = [function1,function2,function3]
   data_sets = [data,data,data]

If a function returns a value, those values will overwrite the source datasets. Similarly, each subsequent function will
use this data that has been returned by the function before it.

Args:
   data_sets - accepts a list. Each element is a data set.
   functions - accepts a list. Each element is a function.
   cores - accepts an integer. It represents the number of individual processes to create.
   chunk - accepts a boolean. If set to True, breaks the data_sets[0] into chunks to be processed.

Returns:
   data_sets - a list where the returns of the last function are appended. Each element cooresponds to the data_set.

Additional Comments:
       Make sure to include " if __name__ == "__main__": " before any non-definitional code in your script before calling this function.

Example Code:

    def double_me(list1):
        new_list = []
        for num in list1:
            num *= 2
            new_list.append(num)
        return new_list
    
    if __name__ == "__main__":
        data = [0,1,2,3,4,5,6,7]
        data1 = [0,1,2,3,4,5,6,7]
        data_sets = [data,data1]
        functions = [double_me]
        data_sets = parallel_by_function(data_sets, functions, cores=2, chunk=False)
        print(data_sets)
"""

from multiprocessing import Pool

def get_length(iterable):
    for index,row in enumerate(iterable):  # @UnusedVariable
        pass
    return index

def parallel_by_function(data_sets, functions, cores=1, chunk=False):
    
    if chunk == True and len(data_sets) != 1:
        raise IndexError("If chunking is enabled, parallel_by_function only accepts a single data set.")
    
    elif chunk == True and len(data_sets) == 1:
        
        for index,row in enumerate(data_sets[0]):  # @UnusedVariable
            length = index
        
        new_data_sets = []
        chunk_amounts = cores * 4

        chunk_size = length / (chunk_amounts)
        
        for unused in range(0,chunk_amounts):
            new_data_sets.append([])
            
        for index in range(0,chunk_amounts):
            append_to = index
            index += 1
            min_range = chunk_size * (index - 1)
            max_range = chunk_size * index
            for index1,item in enumerate(data_sets[0]):
                if min_range <= index1 < max_range:
                    new_data_sets[append_to].append(item)
                elif index1 == max_range and index == chunk_amounts:
                    new_data_sets[append_to].append(item)
            
        data_sets = new_data_sets
    
    with Pool(processes=cores) as working_pool:
    
        for function in functions:
            print('Now Running Function: {}'.format(function))
            data_sets = working_pool.map(function, data_sets)
    
    if chunk == True:
        
        new_data_sets = []
        for item in data_sets:
            for part in item:
                new_data_sets.append(part)
                
        data_sets = new_data_sets
    
    return data_sets
