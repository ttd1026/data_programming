# Data Programming Project

This repository is used to host code for a class project our team took part in in the Spring 2017 semester. The topic was to choose a small project from kaggle.com and attempt to make meaningful steps towards answering the challenged. Due to the time constraints for the project, two weeks, a complete answer to the competition was not expected.

The Kaggle competition can be found here: https://www.kaggle.com/c/acm-sf-chapter-hackathon-big

I. Useful resources/ Recommended Reading:
    1. Scaling up Machine Learning: Parallel and Distributed Approaches, Chapter 16 (available on GSU library)
    2. Collaborative Filtering, Model Based. http://en.wikipedia.org/wiki/Collaborative_filtering#Model-based
    3. GraphLab SVD, Vowpal Wabbit, Mahout, MADlib

II. Some important points about Python style:
    1. Class Name: Class names should normally follow CapsWords convention.
    2. function Name: should be lowercase, with words separated by underscore.
    3. CONSTANTS name: usually in all CAPITAL letters, with underscore separating words
    4. variable name: follow convention for functions
    More on Python style at https://www.python.org/dev/peps/pep-0008/
    
    
  
